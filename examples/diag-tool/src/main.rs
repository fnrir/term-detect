use std::env::var;
use term_detect::{detector::*, DetectionError, Terminal};

fn consolidate(results: Vec<Result<Terminal, DetectionError>>) -> Result<Terminal, DetectionError> {
    let mut res = Vec::new();
    for maybe_term in results {
        if maybe_term.is_ok() {
            return maybe_term;
        }
        let e = maybe_term.unwrap_err();
        res.push(e);
    }
    Err(res.into())
}

fn main() {
    println!("term-detect diagnosis tool v{}", env!("CARGO_PKG_VERSION"));
    let results = [
        detect_terminal_env(),
        detect_terminal_xdg_proposed(),
        detect_terminal_desktop(),
        detect_terminal_path(),
    ];
    println!(
        "Desktop Environment: {}",
        var("XDG_CURRENT_DESKTOP").unwrap_or_default()
    );
    println!(
        "Desktop Session: {}",
        var("XDG_SESSION_DESKTOP").unwrap_or_default()
    );
    println!("Results:");
    for (n, res) in results.iter().enumerate() {
        println!("  Method {}: {:?}", n + 1, res);
    }
    let res = consolidate(results.into());
    println!("Final result: {:?}", res.ok());
}
