use std::ffi::OsStr;

/// Simple wrapper around a String
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Terminal(pub String);

impl Default for Terminal {
    fn default() -> Self {
        Terminal("xterm".to_owned())
    }
}

impl From<String> for Terminal {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl From<&str> for Terminal {
    fn from(value: &str) -> Self {
        Self(value.to_owned())
    }
}

impl AsRef<OsStr> for Terminal {
    fn as_ref(&self) -> &OsStr {
        OsStr::new(&self.0)
    }
}
