use std::{env::VarError, io::Error as IoError};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum DetectionError {
    #[error("environment var error")]
    VarError(#[from] VarError),
    #[error("child process failed")]
    IoError(#[from] IoError),
    #[error("the desktop file does not contain Exec key")]
    DesktopFileError(String),
    #[error("unsupported desktop")]
    UnsupportedDesktopError,
    #[error("failed to parse config file")]
    IniParseError(String),
    #[error("terminal not found in desktop config")]
    DEFindError,
    #[error("no known terminal found in path")]
    FindError,
    #[error("failed to detect terminal")]
    MetaError(Vec<DetectionError>),
}

impl From<Vec<DetectionError>> for DetectionError {
    fn from(value: Vec<DetectionError>) -> Self {
        Self::MetaError(value)
    }
}
