#[cfg(feature = "cache")]
pub mod cache;
#[cfg(feature = "cmdtrait")]
mod cmdtrait;
pub mod desktops;
pub mod detector;
mod error;
mod structs;
pub(crate) mod xdg;

#[cfg(feature = "cache")]
use cache::*;
#[cfg(feature = "cmdtrait")]
pub use cmdtrait::*;
use detector::*;
pub use error::DetectionError;
pub use structs::Terminal;

/// Gets the default terminal  
///
/// If the `cache` feature is enabled, the result is cached  
/// The cache can be reset with `cache::cache_clear`
pub fn get_terminal() -> Result<Terminal, DetectionError> {
    #[cfg(feature = "cache")]
    if let Ok(term) = cache_get() {
        return Ok(term);
    }
    let term = detect_terminal();
    #[cfg(feature = "cache")]
    if let Ok(term) = term.as_ref() {
        let _ = cache_set(term.clone());
    };
    term
}

#[doc = include_str!("../README.md")]
#[cfg(doctest)]
pub struct ReadmeDoctests;
