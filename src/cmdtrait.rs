use crate::{get_terminal, DetectionError, Terminal};
use std::ffi::OsStr;
use std::process::Command;

#[cfg(target_os = "linux")]
fn rawstrcmd(cmd: &Command) -> String {
    format!("{:?}", cmd)
}

#[cfg(target_os = "linux")]
fn strcmd(cmd: &Command) -> String {
    format!("bash -c {:?}", rawstrcmd(cmd))
}

/// Allows a `Command` to be executed in a terminal
pub trait InTerminal {
    fn in_terminal(&self, term: Terminal) -> Self;
    fn in_terminal_args<I, S>(&self, term: Terminal, args: I) -> Self
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>;
}

impl InTerminal for Command {
    fn in_terminal(&self, term: Terminal) -> Self {
        let mut cmd = Command::new(term);
        cmd.arg("-e").arg(strcmd(self));
        cmd
    }
    fn in_terminal_args<I, S>(&self, term: Terminal, args: I) -> Self
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        let mut cmd = Command::new(term);
        cmd.args(args).arg("-e").arg(strcmd(self));
        cmd
    }
}

/// Allows a `Command` to be executed in a terminal (which is automatically detected)
pub trait InTerminalAuto: InTerminal
where
    Self: Sized,
{
    fn in_terminal(&self) -> Result<Self, DetectionError>;
    fn in_terminal_args<I, S>(&self, args: I) -> Result<Self, DetectionError>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>;
}

impl InTerminalAuto for Command {
    fn in_terminal(&self) -> Result<Self, DetectionError> {
        Ok(InTerminal::in_terminal(self, get_terminal()?))
    }
    fn in_terminal_args<I, S>(&self, args: I) -> Result<Self, DetectionError>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        Ok(InTerminal::in_terminal_args(self, get_terminal()?, args))
    }
}
