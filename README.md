# term-detect

Terminal emulator detector

## Implementation details

It works using 3 methods.

1. `TERMINAL` environment variable  
    The terminal is detected by reading the `TERMINAL` environment variable.
1. DE (desktop environment) specific checks  
    The terminal is detected by checking DE-specific config files.
1. `$PATH` search  
    The terminal from a list is picked if it's found in `$PATH`.

## Examples

Run fish in private mode in a terminal window.
Don't close the window when the command ends.

```rust
use std::process::Command;
use term_detect::{DetectionError, InTerminalAuto};

fn main() -> Result<(), DetectionError> {
    let child = Command::new("fish")
        .arg("--private")
        // Konsole-specific flag
        .in_terminal_args(["--noclose"])?
        .spawn()?
        .wait()?;
    Ok(())
}
```
